#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include "TFile.h"
#include "TDirectory.h"
#include <TDirectoryFile.h>
#include <TCanvas.h>
#include <TKey.h>
#include "TPad.h"
#include "TList.h"
#include "TMultiGraph.h"
#include "TGraph.h"
#include "TH1.h"
#include "TH1F.h"
#include "TTree.h"
#include <TRandom.h>
#include <Riostream.h>

int minX = 1565301000; //31_5_2(1565300884, 374.2) 
int cr, sl, fi;
Int_t crate, slot, fiber;
Int_t cra, slo, fib, npoints;
Double_t x, y, mean, rms;
// To store NPoints
std::vector<Int_t>* nPointsAfteCut = new std::vector<Int_t>;
std::vector<std::string>* vecFiberAddress = new std::vector<std::string>;

void storePointInfo(TDirectory* slotDirAccess, TGraph* tgraph, TTree* pointNamu, TTree* fiberNamu) {
  slotDirAccess->cd();
  std::string FiberAddress = "Cr"+std::to_string(cr)+"Sl"+std::to_string(sl)+"Fi"+std::to_string(fi);
  TH1F *histoPerFiber = new TH1F(FiberAddress.c_str(),FiberAddress.c_str(),100000,0.,1000.);
  Double_t xi, yi; 
  std::cout.precision(10);
  // Loop over points to fill TTree
  std::cout << FiberAddress << "_NPoints : " << tgraph->GetN() << std::endl;
  for (int iPoint=0; iPoint<tgraph->GetN(); iPoint++) {
    tgraph->GetPoint(iPoint,xi,yi);
    x = xi;
    y = yi;
    crate = cr;
    slot = sl;
    fiber = fi;
    //std::cout << FiberAddress << "(" << x << ", " << y << ")" << std::endl;
    std::cout << crate << "\t" << slot << "\t" << fiber << "\t(" << x << ", " << y << ")" << std::endl; 
    pointNamu->Fill();
    if (xi<minX) continue;
    histoPerFiber->Fill(y,1.0);
  }
  slotDirAccess->cd();
  histoPerFiber->Write();
  cra = cr;
  slo = sl;
  fib = fi;
  rms = histoPerFiber->GetRMS();
  //std::cout << cra << "\t" << slo << "\t" << fib << "\t" << mean << "\t" <<  rms << std::endl;
  mean = histoPerFiber->GetMean();
  npoints = histoPerFiber->GetEntries();
  if (mean>10) {
    vecFiberAddress->push_back(FiberAddress);
    nPointsAfteCut->push_back(npoints);
    //std::cout << "dk"<< histoPerFiber->GetEntries() <<","<<FiberAddress << std::endl;
  }
  fiberNamu->Fill();
}

int main(int argc, char** argv) {
  std::cout << "\tHello :)" << std::endl;

  ///////////////////////////////////////////////////////////////////////////////////////////////////
  // Create output root file
  TFile *fOut = TFile::Open("hcalPower_out.root","RECREATE");
  // Create pointNamu
  TTree *pointNamu = new TTree("pointNamu","Information per point");
  pointNamu->Branch("crate", &crate);
  pointNamu->Branch("slot", &slot);
  pointNamu->Branch("fiber", &fiber);
  pointNamu->Branch("x", &x);
  pointNamu->Branch("y", &y);
  // Creat fiberNamu
  TTree *fiberNamu = new TTree("fiberNamu", "Information per fiber");
  fiberNamu->Branch("crate", &cra);
  fiberNamu->Branch("slot", &slo);
  fiberNamu->Branch("fiber", &fib);
  fiberNamu->Branch("mean", &mean);
  fiberNamu->Branch("rms", &rms);
  fiberNamu->Branch("npoints", &npoints);
  ///////////////////////////////////////////////////////////////////////////////////////////////////
  
  // Open TDirectoryFile in the root file
  TFile *fileIn = new TFile("hcalPowerAug21.root");
  TDirectory *dir = gFile->GetDirectory("MON_HCAL_UHTR_Fibers_UHTR-POWER");
  // Loop over all object in TDir (CRATEs)
  TIter nextTDirFile (dir->GetListOfKeys());
  TKey *keyTDirFile;
  while ((keyTDirFile = (TKey*)nextTDirFile())) {
    //if (!strstr(keyTDirFile->GetName(),"21")) continue; // for test
    std::string crateName = keyTDirFile->GetName();
    std::cout << "\t:: " << crateName << std::endl; // CRATE #
    cr = std::stoi(crateName.substr(6,crateName.size()));
    crateName.erase(crateName.find(' '), 1);
    //TDirectory *crateDir = fOut->mkdir(crateName.c_str());
    dir->cd(keyTDirFile->GetName());
    // Loop over all TCanvas (SLOTs)
    TDirectory *subdir = gDirectory;
    TIter nextTCanvas (subdir->GetListOfKeys());
    TKey *keyTCanvas;
    while ((keyTCanvas = (TKey*)nextTCanvas())) {
      std::string slotName = keyTCanvas->GetName();
      //if (slotName.find("1") == std::string::npos) continue; // for test
      std::cout << "\t\t|- " << slotName; // SLOT #
      sl = std::stoi(slotName.substr(5,slotName.size()));
      slotName.erase(slotName.find(' '), 1);
      TDirectory *slotDir = fOut->mkdir((crateName+"/"+slotName).c_str());
      // Loop over all objects on TCanvas (continue but for TMultiGraph)
      TCanvas *canvas = (TCanvas*)keyTCanvas->ReadObj();
      TObject *obj;
      TIter nextObj(canvas->GetListOfPrimitives());
      while ((obj=nextObj())) {
	if (!strstr(obj->ClassName(),"TMultiGraph")) continue;
	TMultiGraph *multiGraph = (TMultiGraph*)obj;
	TList *listOfGraphs = (TList*)multiGraph->GetListOfGraphs();
	int fibNumber = listOfGraphs->GetEntries();
	std::cout <<"\t(" << fibNumber << " Fibers)" << std::endl;	
	// Loop over all TGraphs in TMultiGraph (a graph per fiber)
	TGraph *tgraph;
	for ( int i=0; i<fibNumber; i++) {
	  std::string fiberName = listOfGraphs->At(i)->GetName();
	  //if (fiberName.find("9") == std::string::npos || fiberName.find("1") != std::string::npos) continue; // for test
	  std::cout << "\t\t|---- " << fiberName << std::endl; // Fiber #
	  fi = std::stoi(fiberName.substr(6,fiberName.size()));
	  // Push back each TGraph
	  tgraph = (TGraph*) listOfGraphs->At(i)->Clone();
	  TDirectory* slotDirAccess = slotDir->GetDirectory(slotName.c_str());
	  slotDirAccess->cd();
	  tgraph->Write();
	  storePointInfo(slotDirAccess, tgraph, pointNamu, fiberNamu);
	}  // End of fiber loop
      } // End of Canvas object loop
    } // End of Canvas loop
  } // End of CRATE loop

  std::cout << "\n\n Writing TTrees ... " << std::endl;

  fOut->cd();
  pointNamu->Write();
  fiberNamu->Write();
  TH1F *histNpoints = new TH1F("NPoints","NPoints",nPointsAfteCut->size(),0,nPointsAfteCut->size());
  int binIdx = 0;

  for (int i=0;i<nPointsAfteCut->size();i++) {
    if (nPointsAfteCut->at(i)>0) {
    histNpoints->Fill(binIdx,nPointsAfteCut->at(i));
    histNpoints->GetXaxis()->SetBinLabel(binIdx+1, (vecFiberAddress->at(i)).c_str());
    //std::cout << "dy"<<nPointsAfteCut->at(i) <<","<<vecFiberAddress->at(i) << std::endl;
    binIdx++;
    }
  }
  histNpoints->Sumw2();
  std::cout << "Number of valid fibers is " << binIdx << std::endl;
  histNpoints->Write();
  std::cout << "\n\n Closing input... " << std::endl;
  fileIn->Close();
  std::cout << "\n\n Closing output..." << std::endl;
  fOut->Close();
  std::cout << "\n\n DONE \n\n" << std::endl;
}
