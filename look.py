#!/usr/bin/env python

import ROOT as r


def main():
    
    hbhe = " || ".join(["crate == %d" % crate for crate in [20, 21, 24, 25, 30, 31, 34, 35, 37]])
    hbm = "(slot == 1 || slot == 4 || ((slot == 2 || slot == 5) && 1 < fiber && fiber < 11))"
    hbp = "(slot == 7 || slot == 10 || ((slot == 8 || slot == 11) && 1 < fiber && fiber < 11))"
    hb = "(%s) && (%s || %s)" % (hbhe, hbm, hbp)

    f = r.TFile("hcalOpticalPower_MeanRMS_Aug21.root")
    tree = f.Get("fiberNamu")
    tree.SetMarkerStyle(20)
    tree.Draw("rms/mean:mean", hb)
    tree.Scan("crate:slot:fiber:mean:rms", "%s && rms/mean>0.025" % hb)
    f.Close()


if __name__ == "__main__":
    main()
